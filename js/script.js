$(document).on('ready', function () {
	$(".reviews_slider").slick({
		dots: true,
		slidesToShow: 1,
		infinite: true,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: true,
		prevArrow: '',
		nextArrow: '',
		appendDots: $('.reviews_dots'),
		autoplay: true,
      	autoplaySpeed: 2000
	});
	$(".process_slider").slick({
		responsive: [{
				breakpoint: 10000,
				settings: {
					slidesToShow: 2

				}
			}, {
				breakpoint: 767,
				settings: {
					slidesToShow: 1
				}
			}
		],
		slidesToScroll: 1,
		appendArrows: $('.slider_buttons'),
		prevArrow: '<button id="prev" type="button" class="btn btn-juliet"><svg width="51" height="9" viewBox="0 0 51 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.998779 3.9375L6.99878 9L6.99878 0L0.998779 3.9375Z" fill="#1872C5"/><line x1="6.99878" y1="4.5" x2="50.9988" y2="4.5" stroke="#1872C5"/></svg></button>',
		nextArrow: '<button id="next" type="button" class="btn btn-juliet"><svg width="51" height="9" viewBox="0 0 51 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50.9988 5.0625L44.9988 0V9L50.9988 5.0625Z" fill="#1872C5"/><line x1="44.9988" y1="4.5" x2="0.998779" y2="4.5" stroke="#1872C5"/></svg></button>',
		autoplay: true,
      	autoplaySpeed: 2000
	});
	$(".process_slider2").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		appendArrows: $('.slider_buttons2'),
		prevArrow: '<button id="prev" type="button" class="btn btn-juliet"><svg width="51" height="9" viewBox="0 0 51 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.998779 3.9375L6.99878 9L6.99878 0L0.998779 3.9375Z" fill="#1872C5"/><line x1="6.99878" y1="4.5" x2="50.9988" y2="4.5" stroke="#1872C5"/></svg></button>',
		nextArrow: '<button id="next" type="button" class="btn btn-juliet"><svg width="51" height="9" viewBox="0 0 51 9" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M50.9988 5.0625L44.9988 0V9L50.9988 5.0625Z" fill="#1872C5"/><line x1="44.9988" y1="4.5" x2="0.998779" y2="4.5" stroke="#1872C5"/></svg></button>',
		autoplay: true,
      	autoplaySpeed: 2000
	});

	$(".burger").click(function () {
// 		$(".burger").toggleClass("active-burger");
		$(".menu").toggleClass("active-menu");
	});

});
function show(state){
		event.preventDefault();
		document.getElementById('window').style.display = state;			
		document.getElementById('wrap').style.display = state; 			
		}

//Маска

$(document).ready(function() {

$('input[type="tel"]').mask('+7(000)000-00-00');

$(document).on('input keypress keydown', 'input[type="tel"]', function(event){
var input = $(this);
var val = input.val();
var avalibleCode = ['3', '4', '5', '6', '8', '9'];
const which = event.which;

resetError(input);
// console.log('key: ' + which);

if (val.length<=3){
setInputDefault(input);
if (which === 37 || which === 36 || which === 8){ //<-, 'home'
event.preventDefault();
}
}else{// не пустой
if (! avalibleCode.includes(val[3])){
input.val(val.slice(0, 3) + val.slice(4));
showError(input, 'Код города/оператора должен начинаться с цифры 3, 4, 5, 6, 8, 9');
}
}
});


$(document).on('focus, click', 'input[type="tel"]', function(e){
var input = $(this);
e.preventDefault();
// console.log('focus, click');

resetError(input);
var element = document.getElementById(input.attr('id'));

var startPosition = element.selectionStart;

if (startPosition < 3){ //не пускаем перед +7(
input.setCursorPosition(3);
}

if (input.val().length===0){
setInputDefault(input);
}
});

var e = jQuery.Event("keydown"); e.which = 39; // ->
$("#focus").trigger('focus').trigger(e);

$(document).on('submit', '.js-form-valid', function(e){
var form = $(this);
var input = form.find('input[type="tel"]');
var val = input.val();

if (val.length<16) {
showError(input, 'Введите полностью номер телефона!');
e.preventDefault();
}

/* if(true){
'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$';
}*/
});
});


function setInputDefault(input){
input.val('+7(');
}

function showError(input, text){
input.siblings('.js-error-phone').html(text);
}

function resetError(input){
input.siblings('.js-error-phone').html('');
}

$.fn.setCursorPosition = function(pos) {
this.each(function(index, elem) {
if (elem.setSelectionRange) {
elem.setSelectionRange(pos, pos);
} else if (elem.createTextRange) {
var range = elem.createTextRange();
range.collapse(true);
range.moveEnd('character', pos);
range.moveStart('character', pos);
range.select();
}
});
return this;
};/*!
 * GRT Youtube Popup - jQuery Plugin
 * Version: 1.0
 * Author: GRT107
 *
 * Copyright (c) 2017 GRT107
 * Released under the MIT license
*/

$(document).ready(function() {
      $(".youtube-link").grtyoutube({
				autoPlay:true
			});
    });


(function ( $ ) {

	$.fn.grtyoutube = function( options ) {

		return this.each(function() {

			// Get video ID
			var getvideoid = $(this).attr("youtubeid");

			// Default options
			var settings = $.extend({
				videoID: getvideoid,
				autoPlay: true
			}, options );

			// Convert some values
			if(settings.autoPlay === true) { settings.autoPlay = 1 } else { settings.autoPlay = 0 }

			// Initialize on click
			if(getvideoid) {
				$(this).on( "click", function() {
					 $("body").append('<div class="grtvideo-popup">'+
								'<div class="grtvideo-popup-content">'+
									'<span class="grtvideo-popup-close">&times;</span>'+
									'<iframe class="grtyoutube-iframe" src="https://www.youtube.com/embed/'+settings.videoID+'?rel=0&wmode=transparent&autoplay='+settings.autoPlay+'&iv_load_policy=3" allowfullscreen frameborder="0"></iframe>'+
								'</div>'+
							'</div>');
				});
			}

			// Close the box on click or escape
			$(this).on('click', function (event) {
				event.preventDefault();
				$(".grtvideo-popup-close, .grtvideo-popup").click(function(){
					$(".grtvideo-popup").remove();
				});
			});

			$(document).keyup(function(event) {
				if (event.keyCode == 27){
					$(".grtvideo-popup").remove();
				}
			});
		});
	};

}( jQuery ));